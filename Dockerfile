FROM node:alpine 

WORKDIR /app

COPY package*.json ./

ENV NODE_OPTIONS=--max_old_space_size=1024

RUN npm i 

COPY . .

RUN npm run build

EXPOSE 3000

CMD ["npm", "start"]
