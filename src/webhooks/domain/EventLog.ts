

export default class EventLog {
	public date: Date
	public webhookId: string
	public responseStatus: number
	public event: string
	public data?: any

	constructor(
		webhookId: string,
		responseStatus: number,
		event: string,
		data?: any
	){
		this.date = new Date()
		this.webhookId = webhookId
		this.event = event
		this.responseStatus = responseStatus
		if(data) this.data = data;
	}
}
