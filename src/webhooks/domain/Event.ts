
// Formato que recibe la url del webhook
export type FormatedEvent = {
	event: string,
	data: any,
	date: Date
}

export default class Event {

  readonly id?: number;

  readonly date: Date;

  readonly event: string;

  readonly companyId: number;

  readonly data: any;

  constructor(event: string, companyId: number, data: any){
	  this.date = new Date()
	  this.event = event
	  this.companyId = companyId
	  this.data = data
  }

  public format(): FormatedEvent {
	  return {
		  event: this.event, 
		  data: this.data,
		  date: this.date,
	  }
  }
}
