import HttpException from "../../common/HttpException";
import UUDI from "../../common/ID";

export default class Webhook {

    public readonly id: string;
    public active?: boolean;
    public url: string;
    public companyId: number;
    public deleted: boolean; // borrado logico

    constructor(
        companyId: number,
        url: string,
        id?: string,
		active?: boolean
    ){
        this.id = id ? id : new UUDI().string
        this.companyId = companyId
        this.url = url
        this.active = active !== undefined ? active : true
        this.deleted = false
		this.validateUrl()
    }

    public validateUrl(){
            const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
              '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
              '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
              '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
              '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
              '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

            if(!pattern.test(this.url)){
                throw new HttpException(400, 'Invalid URL format')
            }
    }
}

export class WebhookPresenter {
    public readonly id: string;
    public active?: boolean;
    public url: string;
    public companyId: number;

	constructor(webhook: Webhook){
		this.id = webhook.id;
		this.companyId = webhook.companyId;
		this.url = webhook.url;
		this.active = webhook.active;
	}
}
