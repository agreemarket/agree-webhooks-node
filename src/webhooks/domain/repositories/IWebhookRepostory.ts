import { UpdateWebhookDto } from "webhooks/application/UpdateWebhook/UpdateWebhookDto";
import Webhook from "../Webhook";

export type FindParams = {
    companyId: number;
    deleted?: boolean;
}
export type FindOneParams = {
    companyId: number;
    webhookId: string;
}

export default interface IWebhookRepository {
    find(params: FindParams): Promise<Webhook[]>;
    findOne(params: FindOneParams): Promise<Webhook|undefined>;
    create(webhook: Webhook): Promise<void>;
    update(params: UpdateWebhookDto): Promise<Partial<Webhook>|undefined>;
}