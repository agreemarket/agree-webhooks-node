import { injectable } from "inversify";
import IWebhookRepository, { FindOneParams, FindParams } from "webhooks/domain/repositories/IWebhookRepostory";
import Webhook from "../domain/Webhook";
import * as AWS from 'aws-sdk'
import HttpException from "../../common/HttpException";
import { UpdateWebhookDto } from "webhooks/application/UpdateWebhook/UpdateWebhookDto";
import { WEBHOOKS_TABLE } from "@config/environment";

@injectable()
export default class WebhookRepository implements IWebhookRepository {

    private table: string = WEBHOOKS_TABLE
    
    public async create(webhook: Webhook): Promise<void>{

        const DynamoDB = new AWS.DynamoDB.DocumentClient();

        const params = {
            TableName: this.table, 
            Item: {
                companyId: webhook.companyId,
                uuid: webhook.id,
                url: webhook.url,
                active: webhook.active,
				deleted: webhook.deleted
            } 
        };

        try{
            await DynamoDB.put(params).promise()
        }catch(err){
            console.log(err)
            throw new HttpException(500, 'Internal server error')
        }
    }

    public async find(options: FindParams): Promise<Webhook[]> {
        const docClient = new AWS.DynamoDB.DocumentClient();
        
        let params: any = {
            TableName : this.table,
			KeyConditionExpression: "#companyId = :companyId",
			FilterExpression: `#deleted = :deleted`,
            ExpressionAttributeNames:{
                "#companyId": "companyId",
				"#deleted": "deleted",
            },
            ExpressionAttributeValues: {
                ":companyId": options.companyId,
				":deleted": false
            },
        };
		if(options.deleted !== undefined){
			params.ExpressionAttributeValues[':deleted'] = options.deleted
		}
        try{
            const res = await docClient.query(params).promise()

            return res.Items ? res.Items.map(item => new Webhook(item.companyId, item.url, item.uuid, item.active) ) : []     
        }catch(err){
            console.log(err)
            throw new HttpException(500, 'Internal server error')
        }
    }

    public async findOne(options: FindOneParams): Promise<Webhook|undefined> {
        const docClient = new AWS.DynamoDB.DocumentClient();
        
        const params = {
            TableName : this.table,
            Key: {
                companyId: options.companyId,
                uuid: options.webhookId
            }
        };
        try{
            const res = await docClient.get(params).promise()
            return res.Item ? new Webhook(res.Item.companyId, res.Item.url, res.Item.uuid) : undefined
        }catch(err){
            console.log(err)
            throw new HttpException(500, 'Internal server error')
        }
    }

    public async update(options: UpdateWebhookDto): Promise<Partial<Webhook>|undefined> {
        const DynamoDB = new AWS.DynamoDB.DocumentClient();

        const params = {
            TableName: this.table, 
            Key:{
                companyId: options.companyId,
                uuid: options.webhookId
            },
            UpdateExpression: "set",
            ExpressionAttributeNames: {},
            ExpressionAttributeValues: {},
            ReturnValues:"UPDATED_NEW"
        };
        Object.entries(options.data).forEach(([key, item]) => {
            params.UpdateExpression += ` #${key} = :${key},`;
            params.ExpressionAttributeNames[`#${key}`] = key;
            params.ExpressionAttributeValues[`:${key}`] = item
        })
        params.UpdateExpression = params.UpdateExpression.slice(0, -1); 

        try{
            const res = await DynamoDB.update(params).promise()
            return res.Attributes ? res.Attributes : undefined
        }catch(err){
            console.log(err)
            throw new HttpException(500, 'Internal server error')
        }
    }

}
