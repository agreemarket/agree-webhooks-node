import Webhook, { WebhookPresenter } from "@webhooks/domain/Webhook";
import IWebhookRepository, { FindParams } from "webhooks/domain/repositories/IWebhookRepostory";


export default class GetWebhooks {
    
    constructor(
        private repository: IWebhookRepository
    ){}

    public async run(params: FindParams): Promise<WebhookPresenter[]>{
		params.deleted = false
        const result = await this.repository.find(params)
		return result.map((webhook: Webhook) => new WebhookPresenter(webhook))
	}
}
