
export type DeleteWebhookDto = {
    companyId: number,
    webhookId: string
}