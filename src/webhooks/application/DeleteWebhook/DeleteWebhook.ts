import HttpException from "../../../common/HttpException";
import IWebhookRepository from "webhooks/domain/repositories/IWebhookRepostory";
import { DeleteWebhookDto } from "./DeleteWebhookDto";

export default class DeleteWebhook {
    constructor(
        private repository: IWebhookRepository
    ){}

    public async run(params: DeleteWebhookDto){
        const webhook = await this.repository.findOne({ companyId: params.companyId, webhookId: params.webhookId })
        if(!webhook) throw new HttpException(404, 'Wehbook not found')

        await this.repository.update({
            companyId: webhook.companyId,
            webhookId: webhook.id,
            data: {
                deleted: true
            }
        })
    }
}