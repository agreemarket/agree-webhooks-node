import Webhook from "../../domain/Webhook";
import Event, { FormatedEvent } from "../../domain/Event";
import IWebhookRepository from "../../domain/repositories/IWebhookRepostory";
import { SendEventDto } from "./SendEventDto";
import axios from 'axios'
import EventLog from "../../domain/EventLog";
import Logger from "@config/logger";

export default class SendEvent {

	constructor(
		private repository: IWebhookRepository
	){ }

	public async run(eventData: SendEventDto): Promise<Event|undefined>{

		const webhooks: Webhook[] = await this.repository.find({ companyId: eventData.companyId })

		if(!webhooks.length){
			return
		}

		const event = new Event(
			eventData.event,
			eventData.companyId,
			eventData.data,
		)

		webhooks.forEach(async (webhook: Webhook) => {
			if(webhook.active){
				await this.postEvent(webhook, event.format())
			}
		})

		return event
	}

	public async postEvent(webhook: Webhook, data: FormatedEvent){
		let log: EventLog;
		try{
			const res = await axios.post(webhook.url, data )
			log = new EventLog(
				webhook.id,
				res.status,
				data.event,
				data.data
			)
			Logger.info('EVENT SENT: ' + JSON.stringify(log))
			return log
		}catch(err: any){
			let status = err.response ? err.response.status : 404
			log = new EventLog(
				webhook.id,
				status,
				data.event,
				data.data
			)
			Logger.info('EVENT SENT: ' + JSON.stringify(log))
			return log
		}
	}
}
