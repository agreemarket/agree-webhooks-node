import { mock } from 'jest-mock-extended'
import IWebhookRepository from '../../../domain/repositories/IWebhookRepostory'
import SendEvent from "../SendEvent"
import { SendEventDto } from '../SendEventDto'
import Webhook from '../../../domain/Webhook'
import Event from '../../../domain/Event'
import EventLog from '../../../domain/EventLog'

describe('Send events to company webhooks', () => {

	const repository = mock<IWebhookRepository>()

	test('Send event', async () => {

		const eventData: SendEventDto = {
			companyId: 123,
			event: 'ORDER_CREATED',
			data: { test: "test" }
		}
		const webhooks: Webhook[] = []
		webhooks.push(new Webhook(123, 'https://test.com'))
		webhooks.push(new Webhook(123, 'https://url.com'))

		repository.find.mockReturnValue(Promise.resolve(webhooks))
		 
		const useCase = new SendEvent(repository)

		const event = await useCase.run(eventData)
		
		expect(event).toBeInstanceOf(Event)
	})

	test('Send event to company without wehbooks, should return undefined', async () => {

		const eventData: SendEventDto = {
			companyId: 123,
			event: 'ORDER_CREATED',
			data: { test: "test" }
		}
		const webhooks: Webhook[] = []

		repository.find.mockReturnValue(Promise.resolve(webhooks))
		 
		const useCase = new SendEvent(repository)

		const event = await useCase.run(eventData)
		
		expect(event).toBeUndefined()
	})

	// TODO timeout
	test.skip('When sending and event should return an Event Log', async () => {

		const webhook = new Webhook(123, 'https://test.com')
		 
		const useCase = new SendEvent(repository)

		const data = new Event('TEST_EVENT', 666, { test: "test" })

		expect(await useCase.postEvent(webhook, data)).toBeInstanceOf(EventLog)
	})

})
