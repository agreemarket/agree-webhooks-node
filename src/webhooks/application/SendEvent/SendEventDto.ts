
export type SendEventDto = {
	companyId: number,
	event: string,
	data: any
}
