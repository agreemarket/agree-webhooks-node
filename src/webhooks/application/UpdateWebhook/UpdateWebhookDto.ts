import Webhook from "../../domain/Webhook";

export type UpdateWebhookDto = {
    companyId: number,
    webhookId: string,
    data: Partial<Webhook>
}