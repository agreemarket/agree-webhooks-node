import HttpException from "../../../common/HttpException";
import IWebhookRepository from "webhooks/domain/repositories/IWebhookRepostory";
import { UpdateWebhookDto } from "./UpdateWebhookDto";


export default class UpdateWebhook {

    constructor(
        private repository: IWebhookRepository
    ){}

    public async run(params: UpdateWebhookDto){
        const webhook = await this.repository.findOne({ companyId: params.companyId, webhookId: params.webhookId })
        if(!webhook) throw new HttpException(404, 'Wehbook not found')

        return await this.repository.update(params)
    }
}
