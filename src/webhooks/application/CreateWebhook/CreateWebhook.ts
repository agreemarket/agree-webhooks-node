import IWebhookRepository from "../../domain/repositories/IWebhookRepostory";
import Webhook, { WebhookPresenter } from "../../domain/Webhook";
import { CreateWebhookDto } from "./CreateWebhookDto";

export default class CreateWebhook {

    constructor(
        private respository: IWebhookRepository,
    ){}

    public async run(data: CreateWebhookDto): Promise<WebhookPresenter>{

        const webhook = new Webhook(data.companyId, data.url)
        
        const result = await this.respository.create(webhook)

        return new WebhookPresenter(webhook)
    }
}
