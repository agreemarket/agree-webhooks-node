
export type CreateWebhookDto = {
    url: string;
    companyId: number;
    active?: boolean;
}