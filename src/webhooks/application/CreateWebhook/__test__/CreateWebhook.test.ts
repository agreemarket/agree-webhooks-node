import CreateWebhook from '../CreateWebhook'
import { CreateWebhookDto } from '../CreateWebhookDto'
import IWebhookRepository from '../../../domain/repositories/IWebhookRepostory'
import { mock } from 'jest-mock-extended'
import { WebhookPresenter } from '../../../domain/Webhook'
import HttpException from '../../../../common/HttpException'

describe('Create webhooks tests', () => {

    const repository = mock<IWebhookRepository>()

    test('Create valid webhook', async () => {

        const params: CreateWebhookDto = {
            companyId: 123,
            url: 'https://agree.ag'
        }

        const webhook = await new CreateWebhook(repository).run(params)
        
        expect(webhook).toBeInstanceOf(WebhookPresenter)
    })

    test('Throw invalid URL exception', async () => {

        const params: CreateWebhookDto = {
            companyId: 123,
            url: 'test'
        }

        const useCase = new CreateWebhook(repository)

		try{
			await useCase.run(params)
		}catch(err){
			expect(err).toBeInstanceOf(HttpException)
		}
    })
}) 
