import TYPES from "../config/types";
import { inject, injectable } from "inversify";
import CreateWebhook from "./application/CreateWebhook/CreateWebhook";
import { CreateWebhookDto } from "./application/CreateWebhook/CreateWebhookDto";
import IWebhookRepository, { FindParams } from "./domain/repositories/IWebhookRepostory";
import GetWebhooks from "./application/GetWebhooks/GetWebhooks";
import DeleteWebhook from "./application/DeleteWebhook/DeleteWebhook";
import { DeleteWebhookDto } from "./application/DeleteWebhook/DeleteWebhookDto";
import { UpdateWebhookDto } from "./application/UpdateWebhook/UpdateWebhookDto";
import UpdateWebhook from "./application/UpdateWebhook/UpdateWebhook";
import { SendEventDto } from "./application/SendEvent/SendEventDto";
import SendEvent from "./application/SendEvent/SendEvent";
import { WebhookPresenter } from "./domain/Webhook";

@injectable()
export default class WebhookService {

    constructor(
        @inject(TYPES.WebhookRepository) private repository: IWebhookRepository,
    ){}

    public async create(data: CreateWebhookDto): Promise<WebhookPresenter> {
        return await new CreateWebhook(this.repository).run(data);
    }

    public async find(params: FindParams): Promise<WebhookPresenter[]> {
        return await new GetWebhooks(this.repository).run(params)
    }

    public async delete(params: DeleteWebhookDto){
        return await new DeleteWebhook(this.repository).run(params)
    }

    public async update(params: UpdateWebhookDto){
        return await new UpdateWebhook(this.repository).run(params)
    }
	
	public async sendEvent(params: SendEventDto){
		return await new SendEvent(this.repository).run(params)
	}
}
