import ksuid from 'ksuid'

export default class UUDI {

    public readonly string: string
    public readonly obj: ksuid

    constructor(){
        this.obj = ksuid.randomSync(new Date())
        this.string = this.obj.string
    }
}