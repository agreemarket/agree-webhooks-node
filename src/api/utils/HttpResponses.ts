import HttpException from "../../common/HttpException";
import { Response } from "express";

export enum HttpStatus {
    Sucess = 'success',
    Error = 'error',
    Fail = 'fail'    
}

export default class HttpResponse {
    constructor(
        public readonly status: HttpStatus,
        public readonly data?: any,
        public readonly message?: string
    ){}
}

export const errorHandler = (err: any, res: Response) => {
	console.log(err)

	if (err instanceof HttpException) {
		return res
			.status(err.status)
			.json(new HttpResponse(HttpStatus.Fail, { message: err.message }));
	}

	if (err instanceof Error) {
		return res
			.status(500)
			.json(new HttpResponse(HttpStatus.Error, 'Internal server error'));
	}

	return res.status(400).send(new HttpResponse(HttpStatus.Fail, 'Something went wrong'));
}