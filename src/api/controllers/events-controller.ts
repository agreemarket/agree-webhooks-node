import { BaseHttpController, controller, httpDelete, httpGet, httpPost, httpPut, request, response } from "inversify-express-utils";
import { Request, Response } from 'express'
import { inject } from "inversify";
import TYPES from "../../config/types";
import WebhookService from "../../webhooks/WebhookService";
import HttpResponse, { errorHandler, HttpStatus } from "../utils/HttpResponses";
import { SendEventDto } from "webhooks/application/SendEvent/SendEventDto";
import HttpException from "@common/HttpException";

@controller('/webhooks')
export default class EventsController extends BaseHttpController {

    constructor(
        @inject(TYPES.WebhookService) private webhookService: WebhookService,
    ){
        super()
    }

    /**
   * @swagger
   * /webhooks/{company_id}/events:
   *   post:
   *     description: Send event to company webhooks
   *     tags: [Webhooks]
   *     security:
   *       - Bearer: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: company_id
   *         in: path
   *         required: true
   *         type: integer
   *       - name: event
   *         in: formData
   *         example: ORDER_CREATED
   *         required: true
   *         type: string
   *       - name: data
   *         in: formData
   *         required: true
   *         type: object
   *     responses:
   *       201:
   *         description: Webhook
   */
    @httpPost('/:company_id/events')
    public async sendEvent(@request() req: Request, @response() res: Response){
        try{
			if(!req.body.event) throw new HttpException(400, 'Event key is required');
			if(!req.params.company_id) throw new HttpException(400, 'Company ID is required');

			const eventData: SendEventDto = {
				event: req.body.event,
				companyId: parseInt(req.params.company_id),
				data: req.body.data
			}

			const result = await this.webhookService.sendEvent(eventData)
            
            return this.json(new HttpResponse(HttpStatus.Sucess, { event: result }), 200)
        }catch(err){
            return errorHandler(err, res)
        }
    }

}
