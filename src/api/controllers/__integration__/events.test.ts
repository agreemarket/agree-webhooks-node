import request from 'supertest'
import { app } from '../../../index'

describe('Webhooks HTTP Requests', () => {

	const companyId = 123

	it('Should post a new event to company', async () => {

		const params = {
			event: 'TEST_EVENT'
		}

        const req = await request(app)
            .post(`/webhooks/${companyId}/events`)
            .send(params)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
	})
})

