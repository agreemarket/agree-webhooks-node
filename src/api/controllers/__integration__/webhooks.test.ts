
import request from 'supertest'
import { app } from '../../../index'

describe('Webhooks HTTP Requests', () => {

    const companyId = 123

    it('Should create a webhook and return 201', async () => {

        const params = {
            url: "https://agree.ag",
            status: true
        }

        const req = await request(app)
            .post(`/webhooks/${companyId}`)
            .send(params)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)

        expect(req.body.status).toBe('success')
        expect(req.body.data.webhook).toBeDefined()
        expect(req.body.data.webhook.id).toBeDefined()
    })

    it('Should throw an error and return 400', async () => {

        const params = {
            url: "invalid url",
            status: true
        }

        const req = await request(app)
            .post(`/webhooks/${companyId}`)
            .send(params)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(400)

        expect(req.body.status).toBe('fail')
        expect(req.body.data.message).toBeDefined()
    })

    it('Should return 200 with webhooks array', async () => {

        const req = await request(app)
            .get(`/webhooks/${companyId}`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)

        expect(req.body.status).toBe('success')
        expect(req.body.data.webhooks).toBeDefined()
    })
})
