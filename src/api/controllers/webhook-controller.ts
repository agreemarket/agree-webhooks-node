import { BaseHttpController, controller, httpDelete, httpGet, httpPost, httpPut, request, response } from "inversify-express-utils";
import { Request, Response } from 'express'
import { inject } from "inversify";
import TYPES from "../../config/types";
import WebhookService from "../../webhooks/WebhookService";
import { CreateWebhookDto } from "../../webhooks/application/CreateWebhook/CreateWebhookDto";
import HttpResponse, { errorHandler, HttpStatus } from "../utils/HttpResponses";
import HttpException from "../../common/HttpException";
import { DeleteWebhookDto } from "webhooks/application/DeleteWebhook/DeleteWebhookDto";
import { UpdateWebhookDto } from "webhooks/application/UpdateWebhook/UpdateWebhookDto";

@controller('/webhooks')
export default class WebhooksController extends BaseHttpController {

    constructor(
        @inject(TYPES.WebhookService) private webhookService: WebhookService,
    ){
        super()
    }

    /**
   * @swagger
   * /webhooks/{company_id}:
   *   post:
   *     description: Create Webhook
   *     tags: [Webhooks]
   *     security:
   *       - Bearer: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: company_id
   *         in: path
   *         required: true
   *         type: integer
   *       - name: url
   *         in: formData
   *         required: true
   *         type: string
   *       - name: status
   *         in: formData
   *         type: boolean
   *     responses:
   *       201:
   *         description: Webhook
   */
    @httpPost('/:company_id')
    public async create(@request() req: Request, @response() res: Response){
        try{
            if(!req.body.url) throw new HttpException(400, "URL is missing");
            if(!req.params.company_id) throw new HttpException(400, "Company ID is missing");

            const data: CreateWebhookDto = {
                url: req.body.url,
                companyId: parseInt(req.params.company_id),
                active: req.body.status ? req.body.status : true
            }
            
            const result = await this.webhookService.create(data)
            return this.json(new HttpResponse(HttpStatus.Sucess, { webhook: result }), 201)
        }catch(err){
            return errorHandler(err, res)
        }
    }

    /**
   * @swagger
   * /webhooks/{company_id}/{webhook_id}:
   *   put:
   *     description: Delete Webhook
   *     tags: [Webhooks]
   *     security:
   *       - Bearer: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: company_id
   *         in: path
   *         required: true
   *         type: integer
   *       - name: webhook_id
   *         in: path
   *         required: true
   *         type: string
   *       - name: url
   *         in: formData
   *         required: true
   *         type: string
   *       - name: status
   *         in: formData
   *         type: boolean
   *     responses:
   *       200:
   *         description: Status
   */
    @httpPut('/:company_id/:webhook_id')
    public async update(@request() req: Request, @response() res: Response){
        try{
            if(!req.params.company_id) throw new HttpException(400, "Company ID is missing");
            if(!req.params.webhook_id) throw new HttpException(400, "Webhook ID is missing");

            const data: UpdateWebhookDto = {
                companyId: parseInt(req.params.company_id),
                webhookId: req.params.webhook_id,
				data: {}
            }

			if(req.body.url) data.data.url = req.body.url
			if(req.body.active !== undefined) data.data.active = req.body.active
            
            const result = await this.webhookService.update(data)
            return this.json(new HttpResponse(HttpStatus.Sucess, { webhook: result }), 200)
        }catch(err){
            return errorHandler(err, res)
        }
    }

    /**
   * @swagger
   * /webhooks/{company_id}/{webhook_id}:
   *   delete:
   *     description: Delete Webhook
   *     tags: [Webhooks]
   *     security:
   *       - Bearer: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: company_id
   *         in: path
   *         required: true
   *         type: integer
   *       - name: webhook_id
   *         in: path
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: Status
   */
    @httpDelete('/:company_id/:webhook_id')
    public async delete(@request() req: Request, @response() res: Response){
        try{
            if(!req.params.company_id) throw new HttpException(400, "Company ID is missing");
            if(!req.params.webhook_id) throw new HttpException(400, "Webhook ID is missing");
            const params: DeleteWebhookDto = {
                companyId: parseInt(req.params.company_id),
                webhookId: req.params.webhook_id
            }
            const result = await this.webhookService.delete(params)
            return this.json(new HttpResponse(HttpStatus.Sucess, { message: 'Webhook deleted' }))
        }catch(err){
            return errorHandler(err, res)
        }
    }

    /**
   * @swagger
   * /webhooks/{company_id}:
   *   get:
   *     description: Create Webhook
   *     tags: [Webhooks]
   *     security:
   *       - Bearer: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: company_id
   *         in: path
   *         required: true
   *         type: integer
   *     responses:
   *       200:
   *         description: Webhooks
   */
    @httpGet('/:company_id')
    public async get(@request() req: Request, @response() res: Response){
        try{
            if(!req.params.company_id) throw new HttpException(400, 'Company ID is required')
            const webhooks = await this.webhookService.find({ companyId: parseInt(req.params.company_id) })
            return this.json(new HttpResponse(HttpStatus.Sucess, { webhooks } ))
        }catch(err){
            return errorHandler(err, res)
        }
    }

}
