import swaggerJsdoc from 'swagger-jsdoc';
import path from 'path';
const options = {
  swaggerDefinition: {
    info: {
      title: 'Webhooks REST API',
      version: '1.0.0',
      description: 'API Docs'
    }
  },
  apis: [path.join(__dirname, '../api/controllers/*.{ts,js}')]
};
export const specs = swaggerJsdoc(options);