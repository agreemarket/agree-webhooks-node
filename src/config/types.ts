  
const TYPES = {
    WebhookRepository: Symbol.for('WebhookRepository'),
    WebhookService: Symbol.for('WebhookService'),
}

export default TYPES;