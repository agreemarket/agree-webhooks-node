
export const PORT: number = process.env["PORT"] ? parseInt(process.env["PORT"]) : 3000;
export const AWS_ACCESS_KEY: string = process.env['AWS_ACCESS_KEY'] ? process.env['AWS_ACCESS_KEY'] : '';
export const AWS_SECRET_KEY: string = process.env['AWS_SECRET_KEY'] ? process.env['AWS_SECRET_KEY'] : '';
export const AWS_REGION: string = process.env['AWS_REGION'] ? process.env['AWS_REGION'] : '';
export const WEBHOOKS_TABLE: string = process.env['WEBHOOKS_TABLE'] ? process.env['WEBHOOKS_TABLE'] : 'Webhooks.dev';
