import { Container } from "inversify";
import WebhookService from "../webhooks/WebhookService";
import IWebhookRepository from "../webhooks/domain/repositories/IWebhookRepostory";
import WebhookRepository from "../webhooks/infrastructure/WebhookRepository";
import TYPES from "./types";

let container = new Container()

container.bind<IWebhookRepository>(TYPES.WebhookRepository).to(WebhookRepository)
container.bind<WebhookService>(TYPES.WebhookService).to(WebhookService)

export default container