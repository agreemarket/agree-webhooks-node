
import * as AWS from 'aws-sdk'
import { ServiceConfigurationOptions } from 'aws-sdk/lib/service';
import { AWS_ACCESS_KEY, AWS_REGION, AWS_SECRET_KEY } from './environment';

let serviceConfigOptions : ServiceConfigurationOptions = {
    region: AWS_REGION,
    accessKeyId: AWS_ACCESS_KEY,
    secretAccessKey: AWS_SECRET_KEY,
};

AWS.config.update(serviceConfigOptions)