import "reflect-metadata";
import "module-alias/register";
import * as dotenv from 'dotenv'
dotenv.config()
import express, { Request, Response } from 'express'
import { InversifyExpressServer } from "inversify-express-utils";
import container from './config/inversify'
import './api/controllers/index'
import './config/aws'
import { errorHandler } from "./api/utils/HttpResponses";
import swaggerUi from "swagger-ui-express";
import { specs } from "./config/swagger"
import { PORT } from './config/environment'

const server = new InversifyExpressServer(container);
server.setConfig(app => {
	app.options('*', (req, res, next) => { next(); });
	app.use(
		express.urlencoded({
			extended: true
		})
	);
	app.use(express.json());
	app.use (function (err: any, req: any, res: any, next: any){
		return errorHandler(err, res)
	});
});

const app = server.build();
app.use("/docs", swaggerUi.serve, swaggerUi.setup(specs));

const start = async () => {
	try {
		app.listen(PORT, () => console.log(`Server running on port ${PORT}...`));
	} catch (err) {
		throw err
	}
};

start();

export { app }
