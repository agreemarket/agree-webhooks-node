
# Wehooks API

- [Wehooks API](#wehooks-api)
  - [Como iniciar](#como-iniciar)
    - [Docker](#docker)
    - [NPM](#npm)
  - [Tests](#tests)
  - [Estructura de archivos](#estructura-de-archivos)

## Como iniciar

### Docker
- copiar `.env.example` a `.env` y reemplazar las variables
- `docker build -t webhooks .`
- `docker run -d -p 3000:3000 --name webhooks webhooks`

### NPM

- Tener instalado Node.js >= 14.*
- copiar `.env.example` a `.env` y reemplazar las variables
- `npm install`
- `npm run dev` para desarrollo
- `npm run build` para desarrollo
- `npm start` para produccion

La documentacion de swagger esta en: `/docs`

## Tests

- `npm run test` 
- `npm run test:watch` para desarrollo 
- `npm run test:integration` para pruebas de integracion (testea todo el flujo haciendo peticiones http)

## Estructura de archivos
- **index.ts**: entrypoint del servicio y donde esta configurado el servidor
- **api**: contiene los controladores y middlewares para el api rest
- **commmon**: contiene clases y utilidades que se utilizan en todo el proyecto
- **config**: archivos de configuracion (aws, swagger, environment, etc.)
- **webhooks**: el modulo de webhooks, contiene toda la logica del servicio
  - **application**: contiene todos los casos de uso con sus DTO
  - **domain**: contiene todas las entidades e interfaces de repositorios
  - **infrastructure**: contiene la implementacion de los repositorios y cualquier interaccion con librerias externas
