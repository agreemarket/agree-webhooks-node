import paramiko
import os
import sys
import time
from termcolor import colored

if sys.argv[1] == 'DEV':
    server_variable = 'SERVER_DEV'
    branch = 'dev'
elif sys.argv[1] == 'STAGING':
    server_variable = 'SERVER_STAGING'
    branch = 'staging'
elif sys.argv[1] == 'DEMO':
    server_variable = 'SERVER_DEMO'
    branch = 'demo'
elif sys.argv[1] == 'PRODUCTION':
    server_variable = 'SERVER_PRODUCTION'
    branch = 'master'
def runCommand(command):
    print("")
    print(colored(command, 'green'))
    (stdin, stdout, stderr) = client.exec_command(command, get_pty=True)
    for line in stdout:
        print(line, end = '')
    error = stdout.channel.recv_exit_status()
    print(stdout)
    del stdin, stdout, stderr
    print("")
    if (error):
        print(error)
        sys.exit(1)
# Defino la ubicacion del key y me conecto utilizando la librería paramiko
# El hostname lo defino a traves de una variable de entorno, obteniendo el paremetro pasado por bash (por ej SERVER_STAGING)
key = paramiko.RSAKey.from_private_key_file("/opt/atlassian/pipelines/agent/ssh/id_rsa_tmp")
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect( hostname = os.getenv(server_variable), username = os.getenv('USER'), pkey = key )
# --
# Obtengo la carpeta contenedora
folder = os.getenv('FOLDER')

project = "agree-webhooks"

runCommand("git -C " + folder + " checkout " + branch)
runCommand("git -C " + folder + " pull git@bitbucket.org:agreemarket/"+ project +".git " + branch)
runCommand("docker stop " + project + " || true")
runCommand("docker rm " + project + " || true")
runCommand("docker build -t "+ project + " " + folder)
runCommand("docker run --restart always --name " + project + " -p 3208:3000 -d " + project)

# Cierro la conexion
if client is not None:
    client.close()
    del client
# --
