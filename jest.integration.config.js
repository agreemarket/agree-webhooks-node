module.exports = {
	preset: 'ts-jest',
	testEnvironment: 'node',
	verbose: true,
	roots: [
		'<rootDir>/src'
	],
	transform: {
		'^.+\\.ts?$': 'ts-jest'
	},
	testPathIgnorePatterns: ['/__test__/'],
	moduleNameMapper: {
		'@config/(.*)': '<rootDir>/src/config/$1',
		'@webhooks/(.*)': '<rootDir>/src/webhooks/$1',
		'@api/(.*)': '<rootDir>/src/api/$1',
		'@common/(.*)': '<rootdir>/src/common/$1',
	}
};
